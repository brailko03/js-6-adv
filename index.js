"use strict";
// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript


// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const btn = document.querySelector(".btn");

const fetchData = async () => {
    try {
        const result = await fetch("https://api.ipify.org/?format=json")
        const userIP = await result.json()
        // return userIP
        console.log(userIP)

    } catch (err) {
        console.log(err)
    }

}

fetchData()

const fetchData2 = async () => {
    try {
        const result = await fetch("https://ip-api.com//json/userIp")
        const userAdress = await result.json()
        console.log(userAdress)
    } catch (err) {
        console.log(err)
    }

}

fetchData2()
// btn.addEventListener("click", () => {
//     const fetchData = async () => {
//         const result = await fetch("https://api.ipify.org/?format=json")
//         const data = await result.json()
//         console.log(data)
//     }
// })
